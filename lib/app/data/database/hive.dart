import 'dart:async';

import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:poker_cal/app/data/models/player.dart';
import 'package:poker_cal/app/data/models/room.dart';
import 'package:poker_cal/shared/utils/currency.dart';

class HiveDatabase {
  static const String room = 'room';

  static Future<void> initHiveDatabase() async {
    var dir = await getApplicationDocumentsDirectory();
    Hive.init(dir.path);
    Hive.registerAdapter(RoomAdapter());
    Hive.registerAdapter(PlayerAdapter());
  }

  static Future<void> createDatabase() async {
    await Hive.openBox<Room>(room);
  }

  static void addRoom(Room rooms) {
    var _box = Hive.box<Room>(room);
    _box.add(rooms);
  }

  static void addPlayer() {}

  static void updateRoom(indexRoom, indexPlayer) {
    var _box = Hive.box<Room>(room);
    Room? item = _box.getAt(indexRoom);
    item?.listPlayer[indexPlayer].isPaid = true;
    item?.updatedAt = nowDate();
    item?.save();
  }

  static Future<List<Room>> getRoom() async {
    var _box = Hive.box<Room>(room);
    List<Room> listRoom = _box.values.toList();
    return listRoom;
  }

// static void updateFusedPoke(int parent, int mother) {
//   var _index = ((parent - 1) * 151 + mother) - 1;
//   var _box = Hive.box<FusedPoke>(FUSED_POKEMON);
//   FusedPoke? _item = _box.getAt(_index);
//   _item!.isOpened = true;
//   _item.save();
// var _urlAsset = _box.getAt(_index)!.urlAsset;
// var _info = _box.getAt(_index)!.info;
// print(_id);
// print(_urlAsset);
// print(_info);
// _box.putAt(_index,
//     FusedPoke(id: _id, info: _info, isOpened: true, urlAsset: _urlAsset));
// print(_box.getAt(_index)!.isOpened);
// print(_index);
// await box.getAt(index)
// fusedPoke.isOpened = true;
// fusedPoke.save();
}
