import 'package:hive/hive.dart';
part 'player.g.dart';
@HiveType(typeId: 1)
class Player {
  @HiveField(0)
  final int id;

  @HiveField(1)
  String name;

  @HiveField(2)
  int boughtChip;

  @HiveField(3)
  int leftChip;

  @HiveField(4)
  int totalMoney;

  @HiveField(5)
  int sharedChip;

  @HiveField(6)
  bool isPaid;

  Player(
      {this.id = 0,
      this.name = '',
      this.boughtChip = 100,
      this.sharedChip = 0,
      this.leftChip = 0,
      this.totalMoney = 0,
      this.isPaid = false});

  get totalCal => ((leftChip + sharedChip) - boughtChip)  * 1000;
}
