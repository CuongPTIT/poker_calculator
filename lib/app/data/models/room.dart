import 'package:hive/hive.dart';
import 'package:poker_cal/app/data/models/player.dart';

part 'room.g.dart';

@HiveType(typeId: 0)
class Room extends HiveObject {
  @HiveField(0)
  final int id;

  @HiveField(1)
  final String createdAt;

  @HiveField(2)
  String updatedAt;

  @HiveField(3)
  List<Player> listPlayer;

  Room(
      {this.id = 0,
      this.createdAt = '',
      this.updatedAt = '',
      this.listPlayer = const []});
}
