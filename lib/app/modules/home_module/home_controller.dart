import 'package:get/get.dart';
import 'package:poker_cal/app/data/models/room.dart';

import '../../data/database/hive.dart';

class HomeController extends GetxController{

  var listRoom = <Room>[].obs;

  @override
  Future<void> onInit() async {
    await getRoom();
    super.onInit();
  }

  Future getRoom() async {
    listRoom.value = <Room>[];
    await HiveDatabase.getRoom().then((value) {
        listRoom.value = value;
    });
  }
}