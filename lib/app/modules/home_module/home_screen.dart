import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:poker_cal/app/modules/home_module/home_controller.dart';
import 'package:poker_cal/app/modules/home_module/widgets/card_room.dart';
import 'package:poker_cal/app/routes/app_pages.dart';

class HomeScreen extends GetView<HomeController> {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Get.toNamed(Routes.ROOM)?.then((value) => controller.getRoom());
        },
        tooltip: 'Tạo room',
        child: const Icon(Icons.add),
      ),
      body: Obx(() =>  ListView.builder(
        itemCount: controller.listRoom.length,
        itemBuilder: (context, index) {
          return controller.listRoom.isNotEmpty
              ? CardRoom(room: controller.listRoom[index], index: index,)
              : Container();
        },
      ),),
    );
  }
}
