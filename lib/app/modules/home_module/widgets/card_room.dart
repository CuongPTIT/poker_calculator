import 'package:flutter/material.dart';
import 'package:poker_cal/app/data/database/hive.dart';
import 'package:poker_cal/app/data/models/room.dart';
import 'package:poker_cal/shared/utils/currency.dart';

class CardRoom extends StatelessWidget {
  final Room room;
  final int index;

  const CardRoom({Key? key, required this.room, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Phiên: ${room.createdAt}'),
          Text('Cập nhật: ${room.updatedAt}'),
          Row(
            children: const [
              Expanded(
                  flex: 1,
                  child: Text(
                    'Name',
                    textAlign: TextAlign.center,
                  )),
              Expanded(
                  flex: 1,
                  child: Text('Chip mua', textAlign: TextAlign.center)),
              Expanded(
                  flex: 1,
                  child: Text('Chip còn lại', textAlign: TextAlign.center)),
              Expanded(
                  flex: 1,
                  child: Text('Chip chia sẻ', textAlign: TextAlign.center)),
              Expanded(
                  flex: 1,
                  child: Text('Tiền kiếm được', textAlign: TextAlign.center)),
              Expanded(
                  flex: 1,
                  child: Text('Giải ngân', textAlign: TextAlign.center)),
            ],
          ),
          for (var i in room.listPlayer)
            Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Text(
                      i.name,
                      textAlign: TextAlign.center,
                    )),
                Expanded(
                    flex: 1,
                    child: Text(i.boughtChip.toString(),
                        textAlign: TextAlign.center)),
                Expanded(
                    flex: 1,
                    child: Text(i.leftChip.toString(),
                        textAlign: TextAlign.center)),
                Expanded(
                    flex: 1,
                    child: Text(i.sharedChip.toString(),
                        textAlign: TextAlign.center)),
                Expanded(
                    flex: 1,
                    child: Text(
                      decimalNumber(i.totalMoney),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: i.totalMoney > 0 ? Colors.green : Colors.red),
                    )),
                Expanded(
                    flex: 1,
                    child: IconButton(
                      icon:
                          Icon(i.isPaid ? Icons.verified_rounded : Icons.close),
                      onPressed: () {
                        HiveDatabase.updateRoom(index, i.id);
                      },
                    )),
              ],
            )
        ],
      ),
    );
  }
}
