import 'package:get/get.dart';
import 'package:poker_cal/app/modules/room_module/room_controller.dart';

class RoomBinding implements Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => RoomController());
  }

}