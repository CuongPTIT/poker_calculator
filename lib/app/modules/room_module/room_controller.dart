import 'package:get/get.dart';
import 'package:poker_cal/app/data/models/player.dart';

class RoomController extends GetxController{

  var listPlayer = <Player>[].obs;

  var _totalBoughtMoneyRoom = 0.obs;

  set totalBoughtMoneyRoom(value) => _totalBoughtMoneyRoom.value = value;

  get totalBoughtMoneyRoom => _totalBoughtMoneyRoom.value;

  var _totalReceiveMoneyRoom = 0.obs;

  set totalReceiveMoneyRoom(value) => _totalReceiveMoneyRoom.value = value;

  get totalReceiveMoneyRoom => _totalReceiveMoneyRoom.value;

  void addPlayer(id, name){
    listPlayer.add(Player(id: id,name: name));
  }

  @override
  Future<void> onInit() async {
    totalBoughtChip();
    totalReceiveChip();
    super.onInit();
  }
  void totalBoughtChip(){
    totalBoughtMoneyRoom = 0;

    for (var player in listPlayer) {
      totalBoughtMoneyRoom = totalBoughtMoneyRoom + player.boughtChip;
    }
  }

  void totalReceiveChip(){
    totalReceiveMoneyRoom = 0;

    for (var player in listPlayer) {
      totalReceiveMoneyRoom = totalReceiveMoneyRoom + player.leftChip;
    }
  }
}