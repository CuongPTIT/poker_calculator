import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:poker_cal/app/modules/home_module/home_controller.dart';
import 'package:poker_cal/app/modules/room_module/room_controller.dart';
import 'package:poker_cal/app/modules/room_module/widgets/card_player.dart';
import 'package:poker_cal/shared/utils/currency.dart';

import '../../data/database/hive.dart';
import '../../data/models/room.dart';
import 'widgets/dialog_add_player.dart';

class RoomScreen extends GetView<RoomController> {
  const RoomScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int count = -1;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Room'),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: () {
              count++;
              Get.dialog(
                DialogAddPlayer(
                  id: count,
                  controllerRoom: controller,
                ),
              );
            },
            tooltip: 'Tạo room',
            child: const Icon(Icons.add),
          ),
          const SizedBox(height: 30,),
          FloatingActionButton(
            onPressed: () {
              HiveDatabase.addRoom(Room(
                  id: 0,
                  listPlayer: controller.listPlayer,
                  createdAt: nowDate(),
                  updatedAt: nowDate()));
            },
            tooltip: 'Save',
            child: const Icon(Icons.save),
          )
        ],
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(
                  height: 60,
                ),

                Obx(
                  () => ListView.builder(
                    itemCount: controller.listPlayer.length,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return controller.listPlayer.isNotEmpty
                          ? CardPlayer(
                              player: controller.listPlayer[index],
                              controllerRoom: controller,
                            )
                          : Container();
                    },
                  ),
                ),

                // InkWell(
                //   onTap: () => Get.dialog(
                //     DialogAddPlayer(
                //       controllerRoom: controller,
                //     ),
                //   ),
                //   child: Container(
                //     height: 50,
                //     width: 50,
                //     color: Colors.tealAccent,
                //     child: Icon(Icons.add),
                //   ),
                // ),
              ],
            ),
          ),
          Positioned(
            top: 5,
            left: 0,
            right: 0,
            child: SizedBox(
              height: 50,
              child: Card(
                color: Colors.amber,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Obx(
                      () => Text(
                          'Tổng tiền bán: ${decimalNumber(controller.totalBoughtMoneyRoom.abs())}.000 đ'),
                    ),
                    Obx(() => Text(
                        'Tổng tiền thu: ${decimalNumber(controller.totalReceiveMoneyRoom.abs())}.000 đ'))
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
