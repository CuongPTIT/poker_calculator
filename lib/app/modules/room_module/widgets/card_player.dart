import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:poker_cal/app/data/models/player.dart';
import 'package:poker_cal/shared/utils/currency.dart';

import '../../detail_room_module/room_controller.dart';
import '../room_controller.dart';

class CardPlayer extends StatefulWidget {
  final Player player;
  final RoomController controllerRoom;

  const CardPlayer(
      {Key? key, required this.player, required this.controllerRoom})
      : super(key: key);

  @override
  State<CardPlayer> createState() => _CardPlayerState();
}

class _CardPlayerState extends State<CardPlayer> {
  int chipBought = 100;
  int chipLeft = 0;
  int chipShare = 0;
  late TextEditingController controllerLeft;
  late TextEditingController controllerSharing;

  @override
  void initState() {
    chipShare = widget.player.sharedChip;
    chipLeft = widget.player.leftChip;
    // chipBought = widget.player.boughtChip;
    controllerLeft = TextEditingController(text: chipLeft.toString());
    controllerSharing = TextEditingController(text: chipShare.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('id: ${widget.player.id}'),
            Text('Name: ${widget.player.name}'),
            Row(
              children: [
                const Expanded(child: Text('Chip mua:')),
                Row(
                  children: [
                    IconButton(
                        onPressed: () {
                          if (widget.player.boughtChip > 100) {
                            setState(() {
                              // chipBought = chipBought - 100;
                              widget.player.boughtChip =
                                  widget.player.boughtChip - 100;
                              widget.controllerRoom.totalBoughtChip();
                              widget.player.totalMoney = widget.player.totalCal;
                            });
                          }
                        },
                        icon: const Icon(Icons.arrow_back_ios)),
                    Text('${widget.player.boughtChip}'),
                    IconButton(
                        onPressed: () {
                          setState(() {
                            // chipBought = chipBought + 100;
                            widget.player.boughtChip =
                                widget.player.boughtChip + 100;
                            widget.controllerRoom.totalBoughtChip();
                            widget.player.totalMoney = widget.player.totalCal;
                          });
                        },
                        icon: const Icon(Icons.arrow_forward_ios)),
                  ],
                )
              ],
            ),
            Row(
              children: [
                const Expanded(child: Text('Chip dư:')),
                SizedBox(
                  width: Get.width / 2,
                  child: TextField(
                    controller: controllerLeft,
                    onChanged: (val) {
                      setState(() {
                        // chipLeft = parseInt(val);
                        widget.player.leftChip = parseInt(val);
                        widget.controllerRoom.totalReceiveChip();
                        widget.player.totalMoney = widget.player.totalCal;
                      });
                    },
                  ),
                )
              ],
            ),
            Row(
              children: [
                const Expanded(child: Text('Chip chia sẻ:')),
                SizedBox(
                  width: Get.width / 2,
                  child: TextField(
                    controller: controllerSharing,
                    onChanged: (val) {
                      setState(() {
                        // chipShare = parseInt(val);
                        widget.player.sharedChip = parseInt(val);
                        widget.player.totalMoney = widget.player.totalCal;
                      });
                    },
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Text(
              'Tổng tiền: ${decimalNumber(widget.player.totalCal)} đ',
              style: TextStyle(
                  color:
                      widget.player.totalCal > 0 ? Colors.green : Colors.red),
            ),
          ],
        ),
      ),
    );
  }
}
