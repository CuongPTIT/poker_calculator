import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:poker_cal/app/modules/room_module/room_controller.dart';

class DialogAddPlayer extends StatelessWidget {
  final RoomController controllerRoom;
  final int id;

  const DialogAddPlayer({Key? key, required this.controllerRoom, required this.id})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController controller = TextEditingController();
    return Dialog(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            margin: const EdgeInsets.all(10),
            child: TextField(
              decoration: const InputDecoration(hintText: 'Nhập tên'),
              controller: controller,
            ),
          ),
          InkWell(
            onTap: () {
              Get.back();
              controllerRoom.addPlayer(id,controller.text);
              controllerRoom.totalBoughtChip();
            },
            child: Container(
              margin: const EdgeInsets.all(10),
              color: Colors.tealAccent,
              height: 50,
              alignment: Alignment.center,
              width: Get.width / 2,
              child: Text('Thêm'),
            ),
          )
        ],
      ),
    );
  }
}
