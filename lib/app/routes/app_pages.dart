import 'package:get/get.dart';
import 'package:poker_cal/app/modules/home_module/home_bindings.dart';
import 'package:poker_cal/app/modules/home_module/home_screen.dart';
import 'package:poker_cal/app/modules/room_module/room_bindings.dart';
import 'package:poker_cal/app/modules/room_module/room_screen.dart';

part './app_routes.dart';

abstract class AppPages {
  static final pages = [
    GetPage(
      name: Routes.HOME,
      page: () => const HomeScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: Routes.ROOM,
      page: () => const RoomScreen(),
      binding: RoomBinding(),
    ),
  ];
}
