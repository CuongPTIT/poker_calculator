import 'package:intl/intl.dart';
String decimalNumber(int number) =>
      NumberFormat.decimalPattern('vi').format(number);

int parseInt(s) => int.tryParse(s) ?? 0;

String nowDate({String? format}) {
  String date = "";
  DateTime now = DateTime.now();
  date = DateFormat(format ?? 'dd-MM-yyyy').format(now);
  return date;
}
